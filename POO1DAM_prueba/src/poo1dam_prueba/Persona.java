
package poo1dam_prueba;

public class Persona {
    
    String nombre;
    String apellidos;
    String puesto;
    float sueldo;
    float altura;
    int edad;
    String[] vacas=new String[15];
    
    public  String imprimePersona(){
        
        String vacasPropiedad="";
        
        if(!puesto.equals("ordeño")&&!puesto.equals("carniceria")&&!puesto.equals("gerencia")){
            puesto="solo puede ser ordeño, carniceria o gerencia";
            return puesto;
        }
        for(int i=0;i<this.vacas.length;i++){
            vacasPropiedad+=this.vacas[i]+" ";
        }
        return this.nombre+" : "+this.apellidos+" : "+this.edad+" : "+this.altura+"\n"+vacasPropiedad;
    }
    
    public String cuantoGana(){
        return "Gana "+this.sueldo+"€";
    }
    
    public float proporcionSueldo(Persona per){
        return per.sueldo/this.sueldo;
    }
    
    public String ajustaSueldoA(Persona per){
        
        if(this.puesto.equals("ordeño")){
            if(per.puesto.equals("carniceria")){
                this.sueldo=(float)(per.sueldo*1.25);
            }else if(per.puesto.equals("gerencia")){
                this.sueldo=(float)(per.sueldo/1.50);
            }
        }else if(this.puesto.equals("carniceria")){
            if(per.puesto.equals("oreño")){
                this.sueldo=(float)(per.sueldo/1.25);
            }else if(per.puesto.equals("gerencia")){
                this.sueldo=(float)(per.sueldo/1.5/1.25);
            }
        }else{
            if(per.puesto.equals("oreño")){
                this.sueldo=(float)(per.sueldo*1.25);
            }else if(per.puesto.equals("carniceria")){
                this.sueldo=(float)(per.sueldo*1.25*1.5);
            }
        }
        
        return " debería ganar "+this.sueldo+"€";
    }
    
    public void quedarmeVacas(Vaca[] vac){
        
        int cont=0;
        
        for(int i=0;i<vac.length;i++){
            if(this.puesto.equals(vac[i].funcion)){
                this.vacas[cont]=vac[i].nombre;
                cont++;
            }
        }
    }
}
