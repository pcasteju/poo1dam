
package poo1dam_prueba;

public class POO1DAM_prueba {

    /*
por 30xp:

-Añadir a la clase Persona un String "puesto". Usarlo solo para valores "ordeño","carniceria" o "gerencia".
-Añadir a la clase Persona un Float "sueldo", que expresará el sueldo en euros.
-añadir un método cuantoGana a la clase Persona, que devuelva el String "Gana"+sueldo+" €"
-añadir un método proporcionSueldo a la clase Persona, que reciba una persona por argumentos, y devuelva un float con cuántas veces gana esa persona el sueldo de this.
-Añadir un método ajustaSueldoA, que reciba un objeto Persona, y asigne el sueldo de this, poniéndolo en proporción al del objeto que recibe por parámetro. Un/a ordeñador/a debería ganar un 25% más que alguien de carnicería, y un/a gerente un 50% más que alguien de ordeño.
-Crear la clase Vaca, con Strings para nombre, número de serie y función ("ordeño" o "carniceria"), un Float peso, un int edad, y un array de float con litros ordeñados en el día.
-Crear un método asignaFunción a una vaca, de forma que si pesa más de 200kg, se asigne a carnicería, y si pesa menos, a ordeño.
-Añadir a la clase persona un Array de Vacas
-Crear en la clase persona un método quedarmeVacas(Vaca[] vac), que copie en el array interno a las vacas que corresponden a su puesto de trabajo.*/
    
    public static void main(String[] args) {
        
        Vaca[] vac=new Vaca[15];
        Persona p1=new Persona();
        Persona p2=new Persona();
        Vaca v1=new Vaca();
        Vaca v2=new Vaca();
        
        p1.nombre="Ale";
        p1.apellidos="Molina";
        p1.edad=36;
        p1.altura=1.62f;
        p1.puesto="ordeño";
        p1.sueldo=800;
        
        p2.nombre="Pepe";
        p2.apellidos="Castro";
        p2.edad=62;
        p2.altura=1.83f;
        p2.puesto="gerencia";
        p2.sueldo=1500;
        
        v1.nombre="Eugenia";
        v1.edad=5;
        v1.peso=(float)350;
        v1.numSerie="12345A";
        
        v2.nombre="Ramona";
        v2.edad=7;
        v2.peso=(float)167;
        v2.numSerie="12345B";
        
        vac[0]=v1;
        vac[1]=v2;
        
        System.out.println(p1.cuantoGana());
        System.out.println(p1.proporcionSueldo(p2));
        System.out.println(p1.nombre+p1.ajustaSueldoA(p2));
        
        v1.asignaFuncion();
        v2.asignaFuncion();
        
        //p1.quedarmeVacas(vac);
        //p2.quedarmeVacas(vac);
        
        System.out.println(p1.imprimePersona());
        
    }
    
}
